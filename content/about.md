+++
title = "About me"
date = "2021-07-04"
+++

# Me

Hi there, I am a bit surprised to see you on this page. Generally 
speaking, I love abstract conversations and debates. My favourite 
topics to discuss include futurology, philosophy, the universe, and 
psychology.

I am very very organised - it is an obsession. I have realised that 
10% of my thoughts are on the past, 20% are on the present, and 70% 
are on the future. I know that it is hard (and wrong) to put people 
into boxes but I personally believe that the MBTI test doesn't do a bad 
job of helping one understand themselves. I am an INFJ-A presently.

I also really like trying new things specially when they are sports or 
board/card games. Some of my favourites include football, table tennis, 
badminton, chess, catan, coup, avalon, secret hitler, and 
one night werewolf.

# Favourite books

I love reading books and here are my 10 favourite books of all time:

1. Thinking, Fast and Slow
2. AI superpowers
3. How to win friends and influence people
4. Think like a monk
5. The fitness mindset
6. Sherlock Holmes
7. From zero to one
8. Agatha Christie
9. The pragmatic programmer
10. When breath becomes air

# Favourite movies

I also love watching movies and here are my 10 favourite movies of all 
time:

1. Shutter Island
2. 3 Idiots
3. Harry Potter
4. Avengers: Infinity War
5. Parasite
6. The Dark Knight
7. The Sixth Sense
8. Interstellar
9. The Silence of the Lambs
10. Kal ho na ho

I would love to get some book and movie recommendations from you! :)